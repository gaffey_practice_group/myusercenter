package com.gaf.usercenter.service;

import com.gaf.usercenter.model.domain.Myuser;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户服务
 *
* @author gaffey
* @description 针对表【myuser(用户)】的数据库操作Service
* @createDate 2023-10-22 00:31:31
*/
public interface MyuserService extends IService<Myuser> {

	/**
	 * 用户注册
	 * @param myuserAccount 用户账号
	 * @param myuserPassword 用户密码
	 * @param checkPassword 校验密码
	 * @return 新用户id
	 */
	long myuserRegister(String myuserAccount,String myuserPassword,String checkPassword);

	/**
	 * 用户登录
	 *
	 * @param myuserAccount  用户账号
	 * @param myuserPassword 用户密码
	 * @param request
	 * @return 用户信息
	 */
	Myuser myuserLogin(String myuserAccount, String myuserPassword, HttpServletRequest request);

	/**
	 * 用户注销
	 * @param request
	 * @return
	 */
	int myuserLogout(HttpServletRequest request);

	Myuser getSaftyUser(Myuser myuser);
}
