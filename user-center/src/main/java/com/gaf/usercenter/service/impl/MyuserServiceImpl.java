package com.gaf.usercenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gaf.usercenter.common.ErrorCode;
import com.gaf.usercenter.constant.UserConstant;
import com.gaf.usercenter.exception.BusinessException;
import com.gaf.usercenter.model.domain.Myuser;
import com.gaf.usercenter.service.MyuserService;
import com.gaf.usercenter.mapper.MyuserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * 用户实现类
* @author gaffey
* @description 针对表【myuser(用户)】的数据库操作Service实现
* @createDate 2023-10-22 00:31:31
*/
@Service
@Slf4j
public class MyuserServiceImpl extends ServiceImpl<MyuserMapper, Myuser>
    implements MyuserService{


	@Resource
	private MyuserMapper myuserMapper;

	/**
	 * 盐值 混淆密码
	 */
	private static final String SALT = "gaffey";

	@Override
	public long myuserRegister(String myuserAccount, String myuserPassword, String checkPassword) {
		//检验输入
		if(!paramsCheck(myuserAccount,myuserPassword)){
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}

		//校验密码和校验密码相同
		if(!myuserPassword.equals(checkPassword)){
			log.info("校验密码与密码不一致");
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}

		//用户名不能重复
		QueryWrapper<Myuser> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("user_account",myuserAccount);
		long count = myuserMapper.selectCount(queryWrapper);
		if(count > 0){
			log.info("用户名重复");
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}

		//对密码加密
		String encrptPassword = DigestUtils.md5DigestAsHex((SALT + myuserPassword).getBytes());
		//插入数据
		Myuser myuser = new Myuser();
		myuser.setUserAccount(myuserAccount);
		myuser.setUserPassward(encrptPassword);
		boolean saveResult = this.save(myuser);
		if(!saveResult){
			log.error("添加失败");
			throw new BusinessException(ErrorCode.SYSTEM_ERROR);
		}
		return myuser.getId();
	}

	@Override
	public Myuser myuserLogin(String myuserAccount, String myuserPassword, HttpServletRequest request) {
		//检验输入
		if(!paramsCheck(myuserAccount,myuserPassword)){
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}
		//对密码加密
		String encrptPassword = DigestUtils.md5DigestAsHex((SALT + myuserPassword).getBytes());

		QueryWrapper<Myuser> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("user_account",myuserAccount);
		queryWrapper.eq("user_passward",encrptPassword);
		Myuser myuser = myuserMapper.selectOne(queryWrapper);
		//用户不存在
		if(myuser == null){
			System.out.println("not exist");
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}

		Myuser saftyUser = getSaftyUser(myuser);

		//记录用户的登录态
		request.getSession().setAttribute(UserConstant.USER_LOGIN_STATE,saftyUser);

		return saftyUser;

	}

	@Override
	public int myuserLogout(HttpServletRequest request) {
		request.getSession().removeAttribute(UserConstant.USER_LOGIN_STATE);
		return 1;
	}

	@Override
	public Myuser getSaftyUser(Myuser myuser) {
		//信息脱敏
		Myuser saftyuser = new Myuser();
		saftyuser.setId(myuser.getId());
		saftyuser.setUserName(myuser.getUserName());
		saftyuser.setUserAccount(myuser.getUserAccount());
		saftyuser.setAvatarUrl(myuser.getAvatarUrl());
		saftyuser.setGender(myuser.getGender());
		saftyuser.setPhone(myuser.getPhone());
		saftyuser.setEmail(myuser.getEmail());
		saftyuser.setIsAdmin(myuser.getIsAdmin());
		saftyuser.setUserState(myuser.getUserState());
		saftyuser.setCreatetime(myuser.getCreatetime());
		return saftyuser;
	}

	//检验输入
	private boolean paramsCheck(String myuserAccount, String myuserPassword){
		//校验非空
		if(myuserAccount == null || myuserPassword == null){
			log.info("输入有误");
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}

		//账户不小于四位
		if(myuserAccount.length() < 4){
			log.info("账号小于4位");
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}

		//密码不小于8位
		if(myuserPassword.length() < 8){
			log.info("密码小于8位");
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}

		//账户不包含特殊字符
		String validPattern = "[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
		Matcher matcher = Pattern.compile(validPattern).matcher(myuserAccount);
		if (matcher.find()) {
			log.info("账号包含特殊字符");
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}

		return true;
	}

}




