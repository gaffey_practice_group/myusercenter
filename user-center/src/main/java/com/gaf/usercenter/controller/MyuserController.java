package com.gaf.usercenter.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gaf.usercenter.common.BaseResponse;
import com.gaf.usercenter.common.ErrorCode;
import com.gaf.usercenter.common.ResultUtils;
import com.gaf.usercenter.constant.UserConstant;
import com.gaf.usercenter.exception.BusinessException;
import com.gaf.usercenter.model.domain.RegisterLoginUser;
import com.gaf.usercenter.model.domain.Myuser;
import com.gaf.usercenter.service.MyuserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.gson.GsonProperties;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
@CrossOrigin(allowCredentials = "true",originPatterns = "*")
@Slf4j
public class MyuserController {
	@Resource
	private MyuserService myuserService;

	/**
	 * 注册接口
	 * @param registerLoginUser
	 * @return
	 */
	@PostMapping("/register")
	public BaseResponse<Long> userRegister(@RequestBody RegisterLoginUser registerLoginUser){
		//校验参数
		checkParam(registerLoginUser);
		//校验密码和校验密码相同
		if(!registerLoginUser.getUserPassword().equals(registerLoginUser.getCheckPassword())){
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}
		Long userAccount = myuserService.myuserRegister(registerLoginUser.getUserAccount(), registerLoginUser.getUserPassword(),
				registerLoginUser.getCheckPassword());
		if(userAccount < 0){
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}
		return ResultUtils.success(userAccount);
	}

	/**
	 * 登录接口
	 * @param registerLoginUser
	 * @param request
	 * @return
	 */
	@PostMapping("/login")
	public BaseResponse<Myuser> userLogin(@RequestBody RegisterLoginUser registerLoginUser, HttpServletRequest request){
		HttpSession session = request.getSession();
		//校验输入参数
		checkParam(registerLoginUser);
		Myuser myuser = myuserService.myuserLogin(registerLoginUser.getUserAccount(), registerLoginUser.getUserPassword(), request);
		if(myuser == null){
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}
		return ResultUtils.success(myuser);
		
	}

	/**
	 * 用户注销
	 * @param request
	 * @return
	 */
	@PostMapping ("/logout")
	public BaseResponse<Integer> userLogout(HttpServletRequest request){
		if(request == null){
			throw new BusinessException(ErrorCode.NOT_LOGIN);
		}
		int result = myuserService.myuserLogout(request);
		return ResultUtils.success(result);
	}

	/**
	 * 获取当前登录态的用户信息
	 * @param request
	 * @return
	 */
	@GetMapping("/getCurrentUser")
	public BaseResponse<Myuser> getCurrentUser(HttpServletRequest request){
		Object obj = request.getSession().getAttribute(UserConstant.USER_LOGIN_STATE);
		Myuser myuser =(Myuser) obj;
		if(myuser == null){
			throw new BusinessException(ErrorCode.NOT_LOGIN);
		}
		long userId = myuser.getId();
		Myuser myuser1 = myuserService.getById(userId);
		Myuser saftyuser = myuserService.getSaftyUser(myuser1);
		return ResultUtils.success(saftyuser);
	}

	/**
	 * 通过姓名查询用户
	 * @param userName
	 * @param request
	 * @return
	 */
	@PostMapping("/serachUserByName")
	public BaseResponse<List<Myuser>> serchUserByName(String userName,HttpServletRequest request){
		if(!isAdmin(request)){
			throw new BusinessException(ErrorCode.NO_AUTH);
		}
		QueryWrapper<Myuser> queryWrapper = new QueryWrapper<>();
		if (StringUtils.isNotBlank(userName)) {
			queryWrapper.like("user_account", userName);
		}

		List<Myuser> userList =myuserService.list(queryWrapper);
		List<Myuser> list = userList.stream().
				map(myuser -> myuserService.getSaftyUser(myuser)).collect(Collectors.toList());
		return ResultUtils.success(list);
	}

	/**
	 * 通过id删除用户
	 * @param request
	 * @param id
	 * @return
	 */
	@PostMapping("/deleteUser")
	public BaseResponse<Long> deleteUser(HttpServletRequest request,@RequestBody Integer id){
		long userId = (long) id;
		if(!isAdmin(request)){
			throw new BusinessException(ErrorCode.NO_AUTH);
		}
		boolean delete = myuserService.removeById(userId);
		if(!delete){
			throw new BusinessException(ErrorCode.SYSTEM_ERROR);
		}
		return ResultUtils.success(userId);
	}

	/**
	 * 是否为管理员
	 * @param request
	 * @return
	 */
	private boolean isAdmin(HttpServletRequest request){
		Object obj = request.getSession().getAttribute(UserConstant.USER_LOGIN_STATE);
		Myuser currentUser = (Myuser) obj;
		if(currentUser == null){
			//没有登录态
			throw new BusinessException(ErrorCode.NOT_LOGIN);
		}
		Myuser user = myuserService.getById(currentUser.getId());
		if(user.getIsAdmin() != UserConstant.IS_ADMIN){
			//没有权限
			throw new BusinessException(ErrorCode.NO_AUTH);
		}
		return true;
	}

	/**
	 * 校验Param
	 * @param registerLoginUser
	 * @return
	 */
	private boolean checkParam(RegisterLoginUser registerLoginUser){
		//校验非空
		if(StringUtils.isAnyBlank(registerLoginUser.getUserAccount(), registerLoginUser.getUserPassword())){
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}

		//账户不小于四位
		if(registerLoginUser.getUserAccount().length() < 4){
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}

		//密码不小于8位
		if(registerLoginUser.getUserPassword().length() < 8){
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}

		//账户不包含特殊字符
		String validPattern = "[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
		Matcher matcher = Pattern.compile(validPattern).matcher(registerLoginUser.getUserAccount());
		if (matcher.find()) {
			throw new BusinessException(ErrorCode.PARAMS_ERROR);
		}
		return true;
	}

}
