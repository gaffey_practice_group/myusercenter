//package com.gaf.usercenter.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import springfox.documentation.builders.ApiInfoBuilder;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.builders.RequestHandlerSelectors;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;
//
///**
// * 自定义 Swagger 接口文档的配置
// */
//@Configuration
//@EnableSwagger2WebMvc
//public class Knife4jConfiguration {
//
//	@Bean(value = "defaultApi2")
//	public Docket defaultApi2() {
//		Docket docket = new Docket(DocumentationType.SWAGGER_2)
//				.apiInfo(new ApiInfoBuilder()
//						//.title("swagger-bootstrap-ui-demo RESTful APIs")
//						.description("Gaf用户中心API文档")
//						.termsOfServiceUrl("")
//						.contact("SongYj_Gaffe")
//						.version("1.0")
//						.build())
//				//分组名称
////                .groupName("")
//				.select()
//				//这里指定Controller扫描包路径
//				.apis(RequestHandlerSelectors.basePackage("com.gaf.usercenter.controller"))
//				.paths(PathSelectors.any())
//				.build();
//		return docket;
//	}
//}
