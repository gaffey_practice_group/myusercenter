package com.gaf.usercenter.mapper;

import com.gaf.usercenter.model.domain.Myuser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author gaffey
* @description 针对表【myuser(用户)】的数据库操作Mapper
* @createDate 2023-10-22 00:31:31
* @Entity generator.domain.Myuser
*/

public interface MyuserMapper extends BaseMapper<Myuser> {

}




