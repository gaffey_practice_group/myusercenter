package com.gaf.usercenter.model.domain;

import lombok.Data;

@Data
public class RegisterLoginUser {
	private String userAccount;
	private String userPassword;
	private String checkPassword;
}
