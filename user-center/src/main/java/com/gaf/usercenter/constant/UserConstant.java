package com.gaf.usercenter.constant;

public interface UserConstant {
	public static final String USER_LOGIN_STATE = "userLoginState";

	public static final Integer IS_ADMIN = 1;
}
