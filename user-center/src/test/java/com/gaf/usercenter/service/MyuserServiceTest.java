package com.gaf.usercenter.service;

import com.gaf.usercenter.model.domain.Myuser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * 用户服务测试
 */
@SpringBootTest
public class MyuserServiceTest {

	@Resource
	private MyuserService myuserService;

	@Test
	void myuserRegister() {
		String userAccount = "gaffey";
		String userPassword = "12345678";
		String checkPassword = "12345678";
		long result = myuserService.myuserRegister(userAccount,userPassword,checkPassword);
		Assertions.assertNotEquals(-1,result);
		System.out.println(result);
	}

	@Test
	void doLogin() {
		String myuserAccount = "gaffey";
		String myuserPassword = "12345678";

		Myuser myuser = myuserService.myuserLogin(myuserAccount,myuserPassword,null );
		Assertions.assertNotEquals(null,myuser);
		System.out.println(myuser);
	}
}
